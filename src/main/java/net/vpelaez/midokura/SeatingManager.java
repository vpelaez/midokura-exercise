package net.vpelaez.midokura;

public class SeatingManager {
	/* Constructor */

	public SeatingManager(List<Table> tables);

	/* Group arrives and wants to be seated. */
	public void arrives(CustomerGroup group);

	/* Whether seated or not, the group leaves the restaurant. */
	public void leaves(CustomerGroup group);

	/*
	 * Return the table at which the group is seated, or null if they are not seated
	 * (whether they're waiting or already left).
	 */
	public Table locate(CustomerGroup group);
}
